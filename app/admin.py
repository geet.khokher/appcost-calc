'''App Admin'''
from django.contrib import admin

from app.models import Country, EmailMessage


class AdminEmallMessage(admin.ModelAdmin):
    '''
    Admin Class for Email Message
    '''
    models = EmailMessage
    list_display = ['from_email', 'to_email', 'subject',
                    'sent_status', 'sent_date', 'create_date']


class AdminCountry(admin.ModelAdmin):
    '''
    Admin Class for Email Message
    '''
    models = Country
    list_display = ['name', 'phone_code', 'country_code',
                    'country_code_iso3', 'is_active', ]


admin.site.register(EmailMessage, AdminEmallMessage)
admin.site.register(Country, AdminCountry)
