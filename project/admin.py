from django.contrib import admin
from django.contrib import messages as flash_messages
from django.utils import timezone

from .models import (CompanyBilling, CompanyDetail, CompanyDomain,
                     CompanyFramework, CompanyTechnology, Domain, FeatureList,
                     FeatureTimeCostEstimation, Framework, Project,
                     ProjectFeatureList, Technology)


class TechnologyAdmin(admin.ModelAdmin):
    ''' technology admin '''
    model = Technology
    list_display = ('technology_name', 'is_active')


class DomainAdmin(admin.ModelAdmin):
    ''' technology admin '''
    model = Domain
    list_display = ('domain_name', 'domain_description', 'is_active')


class FrameworkAdmin(admin.ModelAdmin):
    ''' technology admin '''
    model = Framework
    list_display = ('framework_name', 'technology', 'is_active')
    list_filter = ('technology',)


class FeatureListAdmin(admin.ModelAdmin):
    ''' technology admin '''
    model = FeatureList
    list_display = ('feature_title', 'feature_type', 'feature_description')
    list_filter = ('feature_type',)


class AdminCompanyBilling(admin.TabularInline):
    '''
    Admin User Interest
    List user's selected interests
    Using in user details view inline
    Tabular inline format to list user interest.
    '''
    model = CompanyBilling
    list_display = ['billing_type', 'cost', 'time']
    extra = 0


class AdminCompanyDomain(admin.TabularInline):
    '''
    Admin User Interest
    List user's selected interests
    Using in user details view inline
    Tabular inline format to list user interest.
    '''
    model = CompanyDomain
    list_display = ['domain', ]
    extra = 0


class AdminCompanyTechnology(admin.TabularInline):
    '''
    Admin User Interest
    List user's selected interests
    Using in user details view inline
    Tabular inline format to list user interest.
    '''
    model = CompanyTechnology
    list_display = ['technology', ]
    extra = 0


class AdminCompanyFramework(admin.TabularInline):
    '''
    Admin User Interest
    List user's selected interests
    Using in user details view inline
    Tabular inline format to list user interest.
    '''
    model = CompanyFramework
    list_display = ['framework', ]
    extra = 0


class CompanyDetailAdmin(admin.ModelAdmin):
    '''Company Detail '''
    model = CompanyDetail
    list_display = ('company_name', 'company_address',
                    'company_owner', 'contact_number', )
    inlines = [AdminCompanyBilling, AdminCompanyDomain,
               AdminCompanyTechnology, AdminCompanyFramework]


class ProjectAdmin(admin.ModelAdmin):
    '''Company Detail '''
    model = Project
    list_display = ('project_title', 'project_owner',
                    'project_code', )


class AdminFeatureTimeCostEstimation(admin.ModelAdmin):
    '''Company Detail '''
    model = FeatureTimeCostEstimation
    list_display = ('company', 'project_domain',
                    'methodlogy', 'technology', 'framework', 'cost', 'time')


admin.site.register(Technology, TechnologyAdmin)
admin.site.register(Domain, DomainAdmin)
admin.site.register(Framework, FrameworkAdmin)
admin.site.register(FeatureList, FeatureListAdmin)
admin.site.register(CompanyDetail, CompanyDetailAdmin)
admin.site.register(FeatureTimeCostEstimation, AdminFeatureTimeCostEstimation)
admin.site.register(Project, ProjectAdmin)
