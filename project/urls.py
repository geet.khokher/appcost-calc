
'''urls configurations for rest api requests'''
from django.conf.urls import url

from .views.api import (AddProjectFeatureAPIView, CreateProjectAPIView,
                        DomainListAPIView, FrameworkListAPIView,
                        GetFullEstimationAPIView, ListCompanyForProjectAPIView,
                        ListFeatureAPIView, ProjectListAPIView,
                        TechnologyListAPIView)

urlpatterns = [
    url(r'^listdomain/$', DomainListAPIView.as_view(), name="domain_list"),
    url(r'^listtechnology/$', TechnologyListAPIView.as_view(), name="technology_list"),
    url(r'^listframework/(?P<id>\d+)/$',
        FrameworkListAPIView.as_view(), name="framework_list"),
    url(r'^listframework/$',
        FrameworkListAPIView.as_view(), name="framework_list_all"),
    url(r'^createproject/$',
        CreateProjectAPIView.as_view(), name="create_project"),
    url(r'^listproject/(?P<owner>\d+)/$',
        ProjectListAPIView.as_view(), name="list_project"),
    url(r'^listcompanyforproject/(?P<domain>\d+)/(?P<tech>\d+)/(?P<frame>\d+)/$',
        ListCompanyForProjectAPIView.as_view(), name="list_company_for_project"),
    url(r'^listcompany/$',
        ListCompanyForProjectAPIView.as_view(), name="list_company"),
    url(r'^companydetail/(?P<id>\d+)/$',
        ListCompanyForProjectAPIView.as_view(), name="detail_company"),
    url(r'^listfeatures/$',
        ListFeatureAPIView.as_view(), name="list_features"),
    url(r'^addprojectfeaturelist/$',
        AddProjectFeatureAPIView.as_view(), name="add_project_feature"),
    url(r'^project-estimation/(?P<id>\d+)/$',
        GetFullEstimationAPIView.as_view(), name="project_est"),

]
