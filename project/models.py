from django.conf import settings
from django.db import models

from app.models import Country


class Domain(models.Model):
    ''' Project Domain '''

    domain_name = models.CharField(max_length=30)
    domain_description = models.CharField(
        max_length=300, blank=True, null=True)
    is_active = models.BooleanField(default=True)
    create_date = models.DateTimeField(auto_now_add=True)
    modify_date = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'Domain'
        verbose_name_plural = 'Domains'

    def __str__(self):
        return self.domain_name


class Technology(models.Model):
    '''technogies'''

    technology_name = models.CharField(max_length=30)
    is_active = models.BooleanField(default=True)
    create_date = models.DateTimeField(auto_now_add=True)
    modify_date = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'Technology'
        verbose_name_plural = 'Technologies'

    def __str__(self):
        return self.technology_name


class Framework(models.Model):
    '''Framework '''
    framework_name = models.CharField(max_length=30)
    technology = models.ForeignKey(
        Technology, related_name='framework_tech')
    is_active = models.BooleanField(default=True)
    create_date = models.DateTimeField(auto_now_add=True)
    modify_date = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'Framework'
        verbose_name_plural = 'Frameworks'

    def __str__(self):
        return self.framework_name


class FeatureList(models.Model):
    '''Project Feature list'''

    AUTHENTICATION = 1
    ONBOARDING = 2
    PROFILE = 3
    EMAIL_NOTIFICATION = 4
    PAYMENT = 5
    PRODUCTS_CART = 6
    ADMIN = 7
    STATIC_PAGE = 8
    OTHERS = 9
    FEATURE_TYPE_CHOICE = (
        (AUTHENTICATION, 'Authentication signup/signin/forget/change password'),
        (ONBOARDING, 'On Boarding'),
        (PROFILE, 'Profile'),
        (EMAIL_NOTIFICATION, 'Email Notification'),
        (PAYMENT, 'Payment'),
        (PRODUCTS_CART, 'products and Cart System'),
        (ADMIN, 'admin'),
        (STATIC_PAGE, 'static pages'),
        (OTHERS, 'others')
    )

    feature_title = models.CharField(max_length=30)
    feature_type = models.SmallIntegerField(
        choices=FEATURE_TYPE_CHOICE)
    feature_description = models.TextField(max_length=500)

    class Meta:
        verbose_name = 'Feature List'
        verbose_name_plural = 'Feature Lists'

    def __str__(self):
        return self.feature_title


class CompanyDetail(models.Model):
    ''' company Detail '''

    company_name = models.CharField(max_length=20)
    company_code = models.CharField(max_length=30)
    company_address = models.CharField(max_length=50)
    company_owner = models.ForeignKey(settings.AUTH_USER_MODEL,
                                      null=True, blank=True, related_name='owner_company')
    contact_number = models.CharField(max_length=18)
    website = models.CharField(max_length=200, null=True, blank=True)
    is_active = models.BooleanField(default=True)
    country = models.ForeignKey(Country, related_name='country')
    create_date = models.DateTimeField(auto_now_add=True)
    modify_date = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'company detail'
        verbose_name_plural = 'company details'

    def __str__(self):
        return self.company_name


class CompanyBilling(models.Model):
    '''Company Resource Cost '''

    FIXED_RESOURCE = 1
    FIXED_COST = 2
    HOURLY = 3
    BILLING_TYPE = (
        (FIXED_RESOURCE, 'Fixed Resource'),
        (FIXED_COST, 'Fixed cost'),
        (HOURLY, 'Hourly')

    )
    company = models.ForeignKey(CompanyDetail, related_name='company_billing')
    billing_type = models.SmallIntegerField(choices=BILLING_TYPE)
    cost = models.FloatField()
    time = models.IntegerField()


class FeatureTimeCostEstimation(models.Model):
    '''
    Time cost estimation
    '''

    WEB_APP = 1
    MOBILE_APP = 2
    BOTH = 3
    ADMIN = 4
    OTHER = 5

    APPLICATION_TYPE_CHOICES = (
        (WEB_APP, 'Web application'),
        (MOBILE_APP, 'MOBILE android/sso/native'),
        (BOTH, 'WEB and Mobile'),
        (ADMIN, 'Admin'),
        (OTHER, 'other')

    )

    AGILE = 1
    WATERFALL = 2
    OTHERS = 3
    METHODLOGY_CHOICES = (
        (AGILE, 'Agile'),
        (WATERFALL, 'waterfall'),
        (OTHERS, 'others')
    )

    company = models.ForeignKey(CompanyDetail, related_name='company_detail')
    project_domain = models.ForeignKey(Domain, related_name='domain')
    feature = models.ForeignKey(FeatureList, related_name='estimated_feature')
    application_type = models.SmallIntegerField(
        choices=APPLICATION_TYPE_CHOICES)
    methodlogy = models.SmallIntegerField(choices=METHODLOGY_CHOICES)
    technology = models.ForeignKey(
        Technology, blank=True, null=True, related_name='project_techno')
    framework = models.ForeignKey(Framework, related_name='framework_feature')
    cost = models.FloatField()
    time = models.IntegerField()
    create_date = models.DateTimeField(auto_now_add=True)
    modify_date = models.DateTimeField(auto_now=True)


class Project(models.Model):
    '''Project '''

    WEB_APP = 1
    MOBILE_APP = 2
    BOTH = 3
    ADMIN = 4
    OTHER = 5

    APPLICATION_TYPE_CHOICES = (
        (WEB_APP, 'Web application'),
        (MOBILE_APP, 'MOBILE android/sso/native'),
        (BOTH, 'WEB and Mobile'),
        (ADMIN, 'Admin'),
        (OTHER, 'other')

    )

    AGILE = 1
    WATERFALL = 2
    OTHERS = 3
    METHODLOGY_CHOICES = (
        (AGILE, 'Agile'),
        (WATERFALL, 'waterfall'),
        (OTHERS, 'others')
    )
    project_code = models.CharField(max_length=30)
    project_title = models.CharField(max_length=30)
    project_description = models.TextField(max_length=500)
    project_owner = models.ForeignKey(
        settings.AUTH_USER_MODEL, related_name='project_owner')
    application_type = models.SmallIntegerField(
        choices=APPLICATION_TYPE_CHOICES)
    methodlogy = models.SmallIntegerField(choices=METHODLOGY_CHOICES)
    project_domain = models.ForeignKey(Domain, related_name='project_domain')
    project_technology = models.ForeignKey(
        Technology, related_name='project_technology')
    project_framework = models.ForeignKey(
        Framework, related_name='project_framework')
    create_date = models.DateTimeField(auto_now_add=True)
    modify_date = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'Project'
        verbose_name_plural = 'Projects'

    def __str__(self):
        return self.project_title


class ProjectFeatureList(models.Model):
    '''
    project feature list
    '''
    project = models.ForeignKey(Project, related_name='project_features')
    feature = models.ForeignKey(
        FeatureTimeCostEstimation, related_name='feature_list')
    create_date = models.DateTimeField(auto_now_add=True)
    modify_date = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'Project Feature'
        verbose_name_plural = 'Project Features'


class CompanyDomain(models.Model):
    '''Company Domain'''
    domain = models.ForeignKey(Domain, related_name='company_domain')
    company = models.ForeignKey(CompanyDetail, related_name='company')

    class Meta:
        verbose_name = 'CompanyDomain'
        verbose_name_plural = 'CompanyDomains'


class CompanyTechnology(models.Model):
    '''Company Domain'''
    technology = models.ForeignKey(
        Technology, related_name='company_technology')
    company = models.ForeignKey(CompanyDetail, related_name='company_tech')

    class Meta:
        verbose_name = 'CompanyTechnology'
        verbose_name_plural = 'CompanyTechnologies'


class CompanyFramework(models.Model):
    '''Company Domain'''
    framework = models.ForeignKey(Framework, related_name='company_framework')
    company = models.ForeignKey(CompanyDetail, related_name='company_frame')

    class Meta:
        verbose_name = 'CompanyFramework'
        verbose_name_plural = 'CompanyFramework'


class ProjectCompanyEstimation(models.Model):
    '''Project Company'''
    project = models.ForeignKey(Project, related_name='est_project')
    company = models.ForeignKey(CompanyDetail, related_name='est_company')
    is_estimated = models.BooleanField(default=False)
    total_cost = models.FloatField(default=0.0, blank=True, null=True)
    total_time = models.IntegerField(default=0.0, blank=True, null=True)
