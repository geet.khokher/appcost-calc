'''
Project serializers

Expanding the usefulness of the serializers is
something that we would like to address. However,
it's not a trivial problem, and it will take some
serious design work.

'''
from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ValidationError
from rest_framework import serializers

from accounts.models import User
from project.models import (CompanyDetail, CompanyDomain, CompanyFramework,
                            CompanyTechnology, Domain, FeatureList,
                            FeatureTimeCostEstimation, Framework, Project,
                            ProjectCompanyEstimation, ProjectFeatureList,
                            Technology)


class GetProjectList(serializers.ModelSerializer):
    ''' Get project '''

    class Meta(object):
        '''meta information'''
        model = Project
        fields = ('__all__')


class GetDomainList(serializers.ModelSerializer):
    ''' Get Domain '''

    class Meta(object):
        '''meta information'''
        model = Domain
        fields = ('id', 'domain_name',)


class GetTechnologyList(serializers.ModelSerializer):
    ''' Get Domain '''

    class Meta(object):
        '''meta information'''
        model = Technology
        fields = ('id', 'technology_name',)


class GetFrameworkList(serializers.ModelSerializer):
    ''' Get Domain '''

    class Meta(object):
        '''meta information'''
        model = Framework
        fields = ('id', 'framework_name', 'technology')


class CreateProjectSerializer(serializers.ModelSerializer):
    ''' create project '''

    def __init__(self, *args, **kwargs):
        super(CreateProjectSerializer, self).__init__(*args, **kwargs)
        context = kwargs.get('context', None)
        if context:
            self.request = kwargs['context']['request']

    class Meta(object):
        '''meta infromation'''
        model = Project
        fields = ('id', 'project_code', 'project_title', 'project_description',
                  'project_domain', 'project_owner', 'project_technology', 'project_framework', 'application_type',
                  'methodlogy')

    def validate(self, validated_data):
        '''
        Validate board name and owner

        '''
        return validated_data

    def create(self, validated_data):
        # validated_data['project_owner'] = self.request.user
        instance = Project.objects.create(**validated_data)
        return instance

    def to_representation(self, instance):
        result = super(CreateProjectSerializer,
                       self).to_representation(instance)
        return result


class CompanyDomainSerializer(serializers.ModelSerializer):

    domain_name = serializers.SerializerMethodField(read_only=True)

    class Meta(object):
        model = CompanyDomain
        fields = ('domain', 'domain_name')

    def get_domain_name(self, instance):
        return instance.domain.domain_name


class CompanyTechnologySerializer(serializers.ModelSerializer):

    technology_name = serializers.SerializerMethodField(read_only=True)

    class Meta(object):
        model = CompanyTechnology
        fields = ('technology', 'technology_name',)

    def get_technology_name(self, instance):
        return instance.technology.technology_name


class CompanyFrameworkSerializer(serializers.ModelSerializer):

    framework_name = serializers.SerializerMethodField(read_only=True)

    class Meta(object):
        model = CompanyFramework
        fields = ('framework', 'framework_name')

    def get_framework_name(self, instance):
        return instance.framework.framework_name


class ListCompanyForProjectSerializer(serializers.ModelSerializer):
    ''' list companies for project'''
    company_domain = serializers.SerializerMethodField(read_only=True)
    company_technology = serializers.SerializerMethodField(read_only=True)
    company_framework = serializers.SerializerMethodField(read_only=True)

    class Meta(object):
        model = CompanyDetail
        fields = ('id', 'company_name', 'company_code', 'company_address',
                  'company_owner', 'contact_number', 'website', 'country',
                  'company_domain', 'company_technology', 'company_framework')

    def get_company_domain(self, instance):
        domain = CompanyDomain.objects.filter(company=instance.pk)
        return CompanyDomainSerializer(domain, many=True).data

    def get_company_technology(self, instance):
        technology = CompanyTechnology.objects.filter(company=instance.pk)
        return CompanyTechnologySerializer(technology, many=True).data

    def get_company_framework(self, instance):
        framework = CompanyFramework.objects.filter(company=instance.pk)
        return CompanyFrameworkSerializer(framework, many=True).data


class FeatureListSerializer(serializers.ModelSerializer):
    ''' Feature list '''

    class Meta(object):
        model = FeatureList
        fields = ('id', 'feature_title', 'feature_type', 'feature_description')


class ProjectFeaturedetailserializer(serializers.ModelSerializer):

    feature = serializers.SerializerMethodField(read_only=True)

    class Meta(object):
        model = FeatureTimeCostEstimation
        fields = ('feature', 'cost', 'time')

    def get_feature(self, instance):
        return FeatureListSerializer(instance.feature.feature).data


class ProjectFeatureserializer(serializers.ModelSerializer):

    feature_list = serializers.SerializerMethodField(read_only=True)

    class Meta(object):
        model = ProjectFeatureList
        fields = ('feature_list',)

    def get_feature_list(self, instance):
        feature_list = ProjectFeatureList.objects.filter(project=instance)
        return ProjectFeaturedetailserializer(feature_list, many=True).data


class AddProjectFeatureSerializer(serializers.ModelSerializer):
    '''Add Model serialiser'''

    feature_list = serializers.ListField(child=serializers.IntegerField(
        min_value=0, max_value=100000), required=False)
    company = serializers.IntegerField()
    domain = serializers.IntegerField()
    methodology = serializers.IntegerField()
    technology = serializers.IntegerField()
    framework = serializers.IntegerField()
    application_type = serializers.IntegerField()

    class Meta(object):
        model = ProjectFeatureList
        fields = ('project', 'feature_list', 'company', 'domain',
                  'methodology', 'framework', 'technology', 'application_type')

    def __init__(self, *args, **kwargs):
        super(AddProjectFeatureSerializer, self).__init__(*args, **kwargs)
        context = kwargs.get('context', None)
        if context:
            self.request = kwargs['context']['request']

    def validate(self, validated_data):
        '''
        Validate board name and owner
        '''
        return validated_data

    def create(self, validated_data):
        instance = None
        total_cost = 0
        total_time = 0
        feature_list = FeatureTimeCostEstimation.objects.filter(feature__in=validated_data['feature_list'],
                                                                company=validated_data['company'],
                                                                technology=validated_data['technology'],
                                                                methodlogy=validated_data['methodology'],
                                                                application_type=validated_data['application_type'],
                                                                project_domain=validated_data['domain'],
                                                                framework=validated_data['framework'])
        for feature in feature_list:
            total_cost = total_cost+feature.cost
            total_time = total_time+feature.time
        obj = ProjectCompanyEstimation.objects.get_or_create(
            company_id=validated_data['company'],
            defaults={"project": validated_data['project']})
        obj[0].total_cost = obj[0].total_cost + total_cost
        obj[0].total_time = obj[0].total_time + total_time
        obj[0].is_estimated = True
        obj[0].save()
        for feature in feature_list.values_list('pk', flat=True):
            instance = ProjectFeatureList.objects.create(project=validated_data['project'],
                                                         feature_id=feature)

        return True

    def to_representation(self, instance):
        return {"message": "jkfhjk"}


class OwnerSerializer(serializers.ModelSerializer):
    ''' list companies for project'''

    class Meta(object):
        model = User
        fields = ('id', 'firstname', 'lastname',
                  'email')


class CompanySerializer(serializers.ModelSerializer):
    ''' list companies for project'''

    owner = serializers.SerializerMethodField(read_only=True)

    class Meta(object):
        model = CompanyDetail
        fields = ('id', 'company_name', 'company_code', 'company_address',
                  'owner', 'contact_number', 'website',
                  )

    def get_owner(self, instance):
        return OwnerSerializer(instance.company_owner).data


class ProjectCompanySerializer(serializers.ModelSerializer):

    company_detail = serializers.SerializerMethodField(read_only=True)

    class Meta(object):
        model = ProjectCompanyEstimation
        fields = ('company_detail', 'total_cost', 'total_time')

    def get_company_detail(self, instance):
        return CompanySerializer(instance.company).data


class GetFullEstimationSerializer(serializers.ModelSerializer):

    company_estimation = serializers.SerializerMethodField(read_only=True)
    project_feature = serializers.SerializerMethodField(read_only=True)
    owner = serializers.SerializerMethodField(read_only=True)

    class Meta(object):
        '''meta '''
        model = Project
        fields = ('project_code', 'project_title',
                  'project_description', 'owner',
                  'company_estimation', 'project_feature')

    def get_owner(self, instance):
        return OwnerSerializer(instance.project_owner).data

    def get_company_estimation(self, instance):
        return ProjectCompanySerializer(instance.est_project.all(), many=True).data

    def get_project_feature(self, instance):
        return ProjectFeatureserializer(instance).data
