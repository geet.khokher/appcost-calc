'''
Api requests for Board module

'''
# pylint: disable=unused-argument, line-too-long
from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.db.models import Case, Count, IntegerField, Max, Q, Sum, When
from django.http import Http404
from rest_framework import generics, status
from rest_framework.response import Response

from appcostcalc.core.permissions import (PrivateTokenAccessPermission,
                                          PublicPrivateTokenAccessPermission)
from project.models import (CompanyDetail, Domain, FeatureList, Framework,
                            Project, Technology)
from project.serializers import (AddProjectFeatureSerializer,
                                 CreateProjectSerializer,
                                 FeatureListSerializer, GetDomainList,
                                 GetFrameworkList, GetFullEstimationSerializer,
                                 GetProjectList, GetTechnologyList,
                                 ListCompanyForProjectSerializer)


class DomainListAPIView(generics.ListAPIView):
    ''' List Domain API '''

    serializer_class = GetDomainList
    queryset = Domain.objects.all()
    # permission_classes = (PrivateTokenAccessPermission,)


class TechnologyListAPIView(generics.ListAPIView):
    ''' List Domain API '''

    serializer_class = GetTechnologyList
    queryset = Technology.objects.filter(is_active=True)
    # permission_classes = (PrivateTokenAccessPermission,)


class ProjectListAPIView(generics.ListAPIView):
    ''' List project API '''

    serializer_class = GetProjectList

    def get_queryset(self):
        return Project.objects.filter(project_owner=self.kwargs['owner'])
    # permission_classes = (PrivateTokenAccessPermission,)


class FrameworkListAPIView(generics.ListAPIView):
    ''' List Domain API '''

    serializer_class = GetFrameworkList
    # permission_classes = (PrivateTokenAccessPermission,)

    def get_queryset(self):
        if 'id' in self.kwargs:
            try:
                tech = Technology.objects.get(pk=self.kwargs['id'])
                return Framework.objects.filter(is_active=True, technology_id=tech.pk)
            except Technology.DoesNotExist():
                pass
        return Framework.objects.filter(is_active=True)


class CreateProjectAPIView(generics.CreateAPIView):
    ''' create project Api '''

    queryset = Project.objects.all()
    serializer_class = CreateProjectSerializer
    # permission_classes = (PrivateTokenAccessPermission, )


class ListCompanyForProjectAPIView(generics.ListAPIView):
    '''
    List API company for project api
    '''
    serializer_class = ListCompanyForProjectSerializer
    # permission_classes = (PrivateTokenAccessPermission,)

    def get_queryset(self):
        if 'domain' and 'tech' and 'frame' in self.kwargs:
            comapany_list = CompanyDetail.objects.filter(
                Q(company_tech__technology__pk=int(self.kwargs['tech'])) |
                Q(company_frame__framework__pk=int(self.kwargs['frame'])) |
                Q(company__domain__pk=int(self.kwargs['domain'])), is_active=True,)
        elif 'id' in self.kwargs:
            comapany_list = CompanyDetail.objects.filter(
                is_active=True, pk=self.kwargs['id'])
        else:
            comapany_list = CompanyDetail.objects.filter(is_active=True)

        return comapany_list


class ListFeatureAPIView(generics.ListAPIView):
    '''list features '''

    queryset = FeatureList.objects.all()
    serializer_class = FeatureListSerializer
    # permission_classes = (PrivateTokenAccessPermission,)


class AddProjectFeatureAPIView(generics.CreateAPIView):
    '''add feature '''

    serializer_class = AddProjectFeatureSerializer
    # permission_classes = (PublicPrivateTokenAccessPermission,)


class GetFullEstimationAPIView(generics.ListAPIView):
    '''estimation'''

    serializer_class = GetFullEstimationSerializer
    # permission_classes = (PublicPrivateTokenAccessPermission,)

    def get_queryset(self):
        project = Project.objects.filter(pk=self.kwargs['id'])
        return project
