'''
Init
'''
# pylint: disable=wildcard-import
try:
    print("Trying import production.py settings...")
    from .production import *
except ImportError:
    try:
        print("Trying import testing.py settings...")
        from .testing import *
    except ImportError:
        print("Trying import development.py settings...")
        from .development import *
