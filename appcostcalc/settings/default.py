'''
Django Default Settings
'''

import os

from django.contrib.messages import constants as messages

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

SECRET_KEY = 'iwk)t*(pxck&&@5_ay1t0lkhx=vykdf&b6f25am@8ix2h*8d0j'

DEBUG = True

ALLOWED_HOSTS = ['*']

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]

THIRD_PARTY_APP = [
    'rest_framework',
    'oauth2_provider',
    'corsheaders',
    'tinymce',
]

LOCAL_APPS = [
    'accounts',
    'app',
    'project',
]

INSTALLED_APPS += THIRD_PARTY_APP + LOCAL_APPS

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'corsheaders.middleware.CorsMiddleware',
]

### REST FRAMEWORK SETTING ###
REST_FRAMEWORK = {
    # 'DEFAULT_RENDERER_CLASSES': (
    #     'appcostcalc.core.renderers.ApiRenderer',
    # ),
    # 'DEFAULT_AUTHENTICATION_CLASSES': (
    #     'oauth2_provider.contrib.rest_framework.OAuth2Authentication',
    # ),
    # # 'EXCEPTION_HANDLER': 'wooclub.core.exceptions.api_exception_handler',
}

APPLICATION_NAME = 'APPCOSTCALC'

# In days.
USER_TOKEN_EXPIRES = 30

ROOT_URLCONF = 'appcostcalc.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

# WSGI_APPLICATION = 'appcostcalc.wsgi.application'

AUTH_USER_MODEL = 'accounts.User'

# Password validation
# https://docs.djangoproject.com/en/1.11/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/1.11/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Email Credentials
EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'nsitester1@gmail.com'
EMAIL_HOST_PASSWORD = 'netsolutions'
EMAIL_DEFAULT = 'nsitester1@gmail.com'

HTTP_API_ERROR = 111

# in minutes
FORGOT_PASSWORD_EXPIRE = 120

EMAIL_VERIFY_EXPIRE = 30

CORS_ORIGIN_ALLOW_ALL = True

STATIC_URL = '/static/'

MEDIA_URL = '/media/'

STATIC_ROOT = os.path.join(BASE_DIR, 'public', 'static')

MEDIA_ROOT = os.path.join(BASE_DIR, 'public', 'media')

ADMIN_PAGE_SIZE = 20

# in minutes
VALIDATE_OTP_EXPIRE = 120

VALIDATE_PASSWORD_MIN = 6

VALIDATE_PASSWORD_MAX = 15

TINYMCE_DEFAULT_CONFIG = {
    'plugins': "table, xhtmlxtras, paste, searchreplace",
    'theme': "advanced",
    "theme_advanced_buttons3_add": "cite,abbr,fontselect,fontsizeselect",
    'cleanup_on_startup': True,
    'custom_undo_redo_levels': 10,
    'height': 500,
    'width': 800,
}

# TINYMCE_SPELLCHECKER = True

X_FRAME_OPTIONS = 'AllowAny'
