'''
Development Environment
'''
from .default import *

DEBUG = True


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.environ.get('DATABASE_NAME', 'dev_appcostcalc'),
        'USER':  os.environ.get('DATABASE_USER', 'postgres'),
        'PASSWORD': os.environ.get('DATABASE_PASSWORD', 'root'),
        'HOST':  os.environ.get('DATABASE_HOST', '127.0.0.1'),
        'PORT': os.environ.get('DATABASE_PORT', '5432')
    }
}

DEBUG_MAIL = ''

EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'nsitester1@gmail.com'
EMAIL_HOST_PASSWORD = 'netsolutions'
EMAIL_DEFAULT = 'nsitester1@gmail.com'


SESSION_COOKIE_HTTPONLY = True

SECURE_CONTENT_TYPE_NOSNIFF = True

SECURE_BROWSER_XSS_FILTER = True

CSRF_COOKIE_HTTPONLY = True

HttpOnly = True

# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases


if DEBUG:

    INSTALLED_APPS += ['log',
                       ]

    MIDDLEWARE += ['log.middleware.LoggingMiddleware']

    LST_APP_FOR_LOGGING = ['app', 'accounts', 'oauth2_provider', ]

    INTERNAL_IPS = ['172.16.16.196', '127.0.0.1']
