'''
Api requests for account module
'''
from collections import OrderedDict

from django.conf import settings
from oauth2_provider.models import AccessToken
from rest_framework import generics, status
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response

from accounts import message
from accounts.models import User, UserSecurityToken
from accounts.serializers import (ChangePasswordSerializer,
                                  CheckUserTokenSerializer,
                                  ForgotPasswordSerializer,
                                  LoginUserSerializer, RegisterUserSerialiser,
                                  ResetPasswordSerializer,
                                  SignOutUserSerializer, ValidateOTPSerializer)
from appcostcalc.core.permissions import (PrivateTokenAccessPermission,
                                          PublicPrivateTokenAccessPermission,
                                          PublicTokenAccessPermission)


class TwoRecordPagination(PageNumberPagination):
    ''' Two Record Pagination '''
    page_size = 20

    def get_paginated_response(self, data):
        return Response(OrderedDict([
            ('next', self.get_next_link()),
            ('previous', self.get_previous_link()),
            ('results', data)
        ]))


class SignUp(generics.CreateAPIView):
    '''
    API for registering new user
    '''
    queryset = User.objects.all()
    serializer_class = RegisterUserSerialiser
    # permission_classes = (PublicTokenAccessPermission, )


class SignIn(generics.CreateAPIView):
    '''handling signin request'''
    queryset = User.objects.all()
    serializer_class = LoginUserSerializer
    # permission_classes = (PublicTokenAccessPermission, )


class SignOut(generics.UpdateAPIView):
    '''
    API for Expire Access Token to Sign out the User
    '''
    queryset = User.objects.all()
    serializer_class = SignOutUserSerializer
    # permission_classes = (PrivateTokenAccessPermission, )

    def get_object(self):
        '''
        Get Object
        '''
        token = self.request.META['HTTP_AUTHORIZATION']
        token_type, token = token.split(' ')
        if token:
            try:
                access_token = AccessToken.objects.filter(token=token).get()
            except AccessToken.DoesNotExist:
                return None
        return access_token

    def update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return super(SignOut, self).update(request, *args, **kwargs)


class ChangePasswordView(generics.UpdateAPIView):
    """
    An endpoint for changing password.
    """
    serializer_class = ChangePasswordSerializer
    permission_classes = (PrivateTokenAccessPermission, )
    #model = User

    def get_object(self):
        obj = self.request.user
        return obj

    def update(self, request, *args, **kwargs):
        self.object = self.get_object()
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            if not self.object.check_password(serializer.data.get("old_password")):
                return Response({"old_password": [message.OLD_PASSOWRD_MISMATCH]},
                                status=status.HTTP_400_BAD_REQUEST)
            self.object.set_password(serializer.data.get("new_password"))
            self.object.save()
            return Response(message.MESSAGE_CHANGE_PASSWORD_SUCCESS, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CheckUserToken(generics.CreateAPIView):
    '''Check User Token expired or not'''
    queryset = User.objects.all()
    serializer_class = CheckUserTokenSerializer
    permission_classes = (PrivateTokenAccessPermission, )


class ForgotPassword(generics.CreateAPIView):
    '''
        API for Forgot Password
    '''
    serializer_class = ForgotPasswordSerializer
    permission_classes = (PublicTokenAccessPermission, )


class ValidateOTP(generics.UpdateAPIView):
    '''
        API for Validation of OTP
    '''
    serializer_class = ValidateOTPSerializer
    permission_classes = (PublicTokenAccessPermission, )

    def get_object(self):
        ''' Get Object '''
        return UserSecurityToken.objects.get(extras=self.request.data['email'],
                                             token=self.request.data['otp'])

    def update(self, request, *args, **kwargs):
        ''' update '''
        try:
            kwargs['partial'] = True
            return super(ValidateOTP, self).update(request, *args, **kwargs)
        except UserSecurityToken.DoesNotExist:
            return Response({'message': message.OTP_INVALID}, status=settings.HTTP_API_ERROR)


class ResetPassword(generics.UpdateAPIView):
    '''
        API for Reset Password
    '''
    serializer_class = ResetPasswordSerializer
    permission_classes = (PublicTokenAccessPermission, )

    def get_object(self):
        return UserSecurityToken.objects.get(extras=self.request.data['email'],
                                             token=self.request.data['otp'])

    def update(self, request, *args, **kwargs):
        try:
            return super(ResetPassword, self).update(request, *args, **kwargs)
        except UserSecurityToken.DoesNotExist:
            return Response({'message': message.OTP_INVALID}, status=settings.HTTP_API_ERROR)
