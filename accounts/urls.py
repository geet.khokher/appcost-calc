'''urls configurations for rest api requests'''
from django.conf.urls import url
from .views.api import (SignUp, SignIn, SignOut, ChangePasswordView, ForgotPassword,
                        ValidateOTP, ResetPassword)

urlpatterns = [
    url(r'^register/$', SignUp.as_view(), name="user_register"),
    url(r'^signin/$', SignIn.as_view(), name="user_signin"),
    url(r'^changepassword/$', ChangePasswordView.as_view(),
        name="user_changepassword"),
    url(r'^signout/$', SignOut.as_view(), name="user_signout"),
    url(r'^forgotpassword/$', ForgotPassword.as_view(),
        name="user_forgotpassword", kwargs={"resend": False}),
    url(r'^validateotp/$', ValidateOTP.as_view(), name="user_validate_otp"),
    url(r'^resetpassword/$', ResetPassword.as_view(), name="user_reset_password"),
]